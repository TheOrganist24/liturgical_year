![TheOrganist24 Code](https://hosted.courtman.me.uk/img/logos/theorganist24_banner_code.png "TheOrganist24 Code")

# Liturgical Year
> Utility for returning lectionary and liturgical calendar days

## Get Started
To install run from inside the directory:
```bash
git clone git@gitlab.com:TheOrganist24/liturgical_year.git
cd liturgical_year
make
make install
```


## Design Notes
![Full Design](img/full_design.png "Full Design")

### Logging Strategy
See [Logging](docs/logging.md).


## Development
### Setting up Environment
```bash
git clone git@gitlab.com:TheOrganist24/liturgical_year.git
cd liturgical_year
git checkout development
make dev
export LOG_LEVEL=INFO  # Optional; supports TRACE, DEBUG, INFO, WARNING, ERROR, CRITICAL
```


### Workflow
1. Review [TODO](TODO.md) list
2. Work off `development` branch
3. Refresh environment `make fresh`
4. Design feature using [TODO](TODO.md) list and/or diagram (`DOCS:` commit)
5. Write tests* (`TEST:` commit)
6. Change or add code (`FUNC:` commit)
7. Update [TODO](TODO.md) list versioning and transfer completed items to [CHANGELOG](CHANGELOG.md)
8. [Version](#Versioning) bump
9. Merge to `main`
10. Tag `git tag -s $(cat VERSION)`


### Versioning
Semantic versioning is used:
```
poetry version <major/minor/patch>
poetry version --short > VERSION
```

Also don't forget to update the [package `__init__.py`](liturgical_year/__init__.py) and [package tests](tests/test_liturgical_year.py).


### Lint and Test
Code should be compliant with PEPs 8, 256, 484, and 526.
```bash
make check  # calls `make lint; make test`
```
