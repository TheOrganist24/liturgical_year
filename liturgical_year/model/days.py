"""Base classes for modelling days of occurrences in the liturgical year."""

from datetime import date, timedelta
from typing import Optional, Union


class Day:
    """Base class for all calendar items in the liturgical year."""

    def __init__(self, dom: int, month: int, year: Optional[int] = None
                 ) -> None:
        """Initialise and validate calendar days."""
        self.dom: int = dom
        self.month: int = month
        self.year: Optional[int] = year

        if not self._validate():
            raise TypeError("Date is invalid")

    def _validate(self):
        """Validate calendar days by substituting a leap year if required."""
        try:
            if not self.year:
                year = 1980  # a leap year to validate all possible dates
            else:
                year = self.year
            dt = date(year, self.month, self.dom)
            return True
        except ValueError:
            return False

    def __str__(self) -> str:
        """Summarise class."""
        return f"Day({self.dom}/{self.month})"


class Sunday(Day):
    """Base class for all Sundays."""

    def __init__(self, dom: int, month: int, year: int) -> None:
        """Reinitialise Day() but with required year."""
        super().__init__(dom, month, year)
        self.year: int = year
        self.dt = date(self.year, self.month, self.dom)


class ObservedDay:
    """Base class for Principal Feasts, Holy Days and Festivals."""

    def __init__(self, name: str, day: Day) -> None:
        """Create named day."""
        self.name: str = name
        self._day = day

    def this(self, year: int = date.today().year) -> date:
        """Return this year's occurrence."""
        dt: date = date(year, self._day.month, self._day.dom)
        return dt


class Sundayphile(ObservedDay):
    """Class to hold days which like to be shifted to Sundays."""

    def __init__(self, name: str, day: Day, forward_shift_limit: int = 0
                 ) -> None:
        """Carry through parent defaults and set the shift limit."""
        self._day: Day = day
        super().__init__(name, self._day)
        self._forward_shift_limit: int = forward_shift_limit

    def _move_to_sunday(self, day: date) -> date:
        """Facilitate Sunday affinity."""
        dt_day = day

        if dt_day.weekday == 6:
            return dt_day

        start: date = dt_day - timedelta(days=(7 - self._forward_shift_limit))
        for shift in range(0, 7):
            potential: date = start + timedelta(days=shift)
            if potential.weekday() == 6:
                return potential
        return potential

    def this(self, year: int = date.today().year) -> date:
        """Return this year's occurrence allowing for shift."""
        dt: date = date(year, self._day.month, self._day.dom)
        dt = self._move_to_sunday(dt)
        return dt


class Easterphile(ObservedDay):
    """Class to hold days which align to Easter."""

    def __init__(self, name: str, days_after: int = 0
                 ) -> None:
        """Carry through parent defaults and set dummy date for Easter."""
        self._day: Day = Day(22, 3)  # earliest possible Easter
        super().__init__(name, self._day)
        self._days_after: int = days_after

    def _calculate_easter(self, year: int = date.today().year) -> date:
        """Calculate Easter for this year."""
        return date(year, self._day.month, self._day.dom)

    def this(self, year: int = date.today().year) -> date:
        """Return this year's occurrence accounting for offset from Easter."""
        dt: date = self._calculate_easter(year) \
            + timedelta(days=self._days_after)
        return dt
