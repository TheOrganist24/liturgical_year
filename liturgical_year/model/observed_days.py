"""Module to provide the High Days and Holidays classes."""

from datetime import date
from typing import Optional

from liturgical_year.model.days import Day, ObservedDay


class Occasion:
    """Base class for Principal Feast Days, Holy Days and Festivals."""

    def __init__(self, observed_day: ObservedDay) -> None:
        """Compose ObservedDay class into self."""
        self._observed_day: ObservedDay = observed_day
        self.name: str = observed_day.name

    def this(self, year: int = date.today().year) -> date:
        """Pass into ObservedDay's mathod."""
        return self._observed_day.this(year)


class PrincipalFeast(Occasion):
    """Class to hold principal feast days."""

    def __init__(self, observed_day: ObservedDay) -> None:
        """Set priority and instantiate parent."""
        super().__init__(observed_day)
        self.priority: Optional[int] = 1
