"""Module to specify all principal feasts and to provide a holder."""

from typing import List

from liturgical_year.model.days import (Day, Easterphile, ObservedDay,
                                        Sundayphile)
from liturgical_year.model.observed_days import PrincipalFeast


class Christmas(PrincipalFeast):
    """Provide definition for Christmas principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = ObservedDay("Christmas Day", Day(25, 12))
        super().__init__(day)


class TheEpiphany(PrincipalFeast):
    """Provide definition for The Epiphany principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = Sundayphile("The Epiphany", Day(6, 1), 3)
        super().__init__(day)


class ThePresentation(PrincipalFeast):
    """Provide definition for The Presentation principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = Sundayphile("The Presentation of Christ in the Temple",
                          Day(2, 2), 2)
        super().__init__(day)


class TheAnnunciation(PrincipalFeast):
    """Provide definition for The Annunciation principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = ObservedDay("The Annunciation of Our Lord to the Blessed Virgin "
                          "Mary", Day(25, 3))
        super().__init__(day)


class Easter(PrincipalFeast):
    """Provide definition for Easter principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = Easterphile("Easter Day")
        super().__init__(day)


class Ascension(PrincipalFeast):
    """Provide definition for Ascension principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = Easterphile("Ascension Day", 39)
        super().__init__(day)


class Pentecost(PrincipalFeast):
    """Provide definition for Pentecost principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = Easterphile("Pentecost (Whit Sunday)", 49)
        super().__init__(day)


class TrinitySunday(PrincipalFeast):
    """Provide definition for Trinity Sunday principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = Easterphile("Trinity Sunday", 56)
        super().__init__(day)


class AllSaints(PrincipalFeast):
    """Provide definition for All Saints principal feast."""

    def __init__(self) -> None:
        """Supply specifics to parent."""
        day = Sundayphile("All Saints Day", Day(1, 11), 5)
        super().__init__(day)


all: List = [
    Christmas(),
    TheEpiphany(),
    ThePresentation(),
    TheAnnunciation(),
    Easter(),
    Ascension(),
    Pentecost(),
    TrinitySunday(),
    AllSaints(),
]
