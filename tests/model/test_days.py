from datetime import date
import pytest

from liturgical_year.model.days import Day, Sunday, ObservedDay


class TestDay():
    def test_accepts_valid_date(self):
        """RBICEP: Right"""
        test_dom = 28
        test_month = 2
        
        test_day = Day(test_dom, test_month)
        
        assert test_dom == test_day.dom \
            and test_month == test_month


    def test_accepts_valid_date_with_year(self):
        """RBICEP: Right"""
        test_dom = 28
        test_month = 2
        test_year = 1970
        
        test_day = Day(test_dom, test_month, test_year)
        
        assert test_year == test_day.year


    def test_accepts_valid_date(self):
        """RBICEP: Error"""
        test_dom = 32
        test_month = 13
        
        with pytest.raises(Exception):
            assert Day(test_dom, test_month)


class TestSunday():
    def test_sunday_is_a_sunday(self):
        """RBICEP: Right"""
        test_dow = 6  # sunday (monday indexed)
        test_dom = 13
        test_month = 2
        test_year = 2022
        
        test_sunday = Sunday(test_dom, test_month, test_year)
        
        assert test_sunday.dt.weekday() == test_dow 


class TestObservedDay():
    def test_this_years_observed_day(self):
        "RBICEP: Right"""
        test_christmas = date(2022, 12, 25)
        test_day = Day(25, 12)
        
        christmas = ObservedDay("Christmas", test_day)
        
        assert christmas.this(2022) == test_christmas
