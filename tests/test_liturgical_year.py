from liturgical_year import __version__


class TestMainModule():
    def test_version(self):
        assert __version__ == "0.1.0"
