# Actions for Liturgical Year
## For 1.0.0
- [ ] Build Sunday name predictor
  - [ ] Calculate seasons
  - [ ] Caluclate high days and holidays
    - [x] Build base Day and Sunday classes
    - [x] Build base ObservedDay class
    - [ ] Calculate Easter
    - [ ] Build each of Principal Feast, Principal Holy Day, and Festival classes
    - [x] Construct Sundayphile and Easterphile classes
    - [ ] Construct all observed:
      - [x] Principal Feasts
      - [ ] Principal Holy Days
      - [ ] Festivals

### Pre-Release Tasks
- [ ] Is code coverage about 95%?
- [ ] Is logging complete?
- [ ] Are all versions correctly set to 1.0.0?
- [ ] Are Docstrings complete?
- [ ] Are the README documentation and diagrams up-to-date and representative?
- [ ] Is there an up-to-date changelog?


## Future
